<?php

namespace GKA\Backpack;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class BackpackProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfigs();

        if ($this->app->runningInConsole()) {
            $this->registerConsoleCommands();
        }

        /*        $this->app->singleton(PathManager::class, function ($app) {
                    $p =  new PathManager();
                    return $p;
                });
        */
        $this->registerPublishableResources();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router, Dispatcher $event)
    {
        //

        /*      $theme = config('noctis.theme', 'bucket');
              $theme_path = dirname(__DIR__, 2) . "/publishable/views/{$theme}";




              $scaffolder_templates = dirname(__DIR__, 2) . "/resources/views/scaffolder";
              $this->loadViewsFrom($scaffolder_templates, "noctis/scaffolder");


              $translation_path = realpath(dirname(__DIR__, 2) . "/publishable/lang");
              $this->loadTranslationsFrom($translation_path, self::$key);

              $this->loadHelpers();*/

        $resource_path = dirname(__DIR__, 1) . "/resources";
        $scaffolder_templates = $resource_path."/views/scaffolder";
        //   dump($scaffolder_templates);
        
        $this->loadViewsFrom($scaffolder_templates, "gka/scaffolder");
        $this->loadViewsFrom($resource_path."/views/crud", "gka/crud");

        $publishablePath = dirname(__DIR__, 1) . '/publishable';

        $this->loadViewsFrom($publishablePath."/views/vendor/backpack/crud", "gka-backpack");
        $this->loadViewsFrom($resource_path."/views", "gka");
    }

    private function registerConsoleCommands()
    {
        /*$this->commands(Commands\ScaffoldCommand::class);
        $this->commands(Commands\InstallCommand::class);
        $this->commands(Commands\PublishThemeCommand::class);
        $this->commands(Commands\Scaffolders\Model::class);*/
        $this->commands(Commands\MigrateCommand::class);
        $this->commands(Commands\Scaffolders\BlueprintModel::class);
        $this->commands(Commands\PublishFieldsCommand::class);
        $this->commands(Commands\PublishPackages::class);
    }


    /**
     * Load helpers.
     */
    protected function loadHelpers()
    {
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }


    public function registerConfigs()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__, 1) . '/config/gka/backpack.php',
            'gka.backpack'
        );
    }


    /**
     * Register the publishable files.
     */
    private function registerPublishableResources()
    {
        $publishablePath = dirname(__DIR__, 1) . '/publishable';

        $packageConfigPath = dirname(__DIR__, 1) . '/config';


        $publishable = [
            'config' => [
                "{$packageConfigPath}/gka" => config_path('gka')
            ],
            'fields' => [
                "{$publishablePath}/views/vendor/backpack/crud/fields" => resource_path('views/vendor/backpack/crud/fields')
            ],
            'packages' => [
                "{$publishablePath}/packages" => public_path('packages')
            ]
        ];
        foreach ($publishable as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }
}
