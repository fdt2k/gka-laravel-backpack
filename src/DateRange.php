<?php

namespace GKA\Backpack;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
class DateRange {


    protected $start;
    protected $end;

    protected $range_start;
    protected $range_end;
    protected $range;

    protected $stringFormat = 'Y-m-d';

    protected $matches = [
        '/Q(?P<quarter>\d)-(?P<year>\d{4})/'=>'match_quarter',
        '/^(?P<year>\d{4})$/'=>'match_year',
        '/^(?P<year>\d{4})-(?P<month>\d{1,2})$/'=>'match_month'
    ];

    static function create (...$args){
        return new DateRange(...$args);
    }




    function __construct($start=null){
        $date = new \Datetime($start);
        $this->day = intval($date->format('j'));
        $this->month = intval($date->format('n'));
        $this->year = intval($date->format('Y'));
        $this->num_days = intval($date->format('t'));
        $this->remaining_days = $this->num_days - $this->day;
    }

    function setRange($start,$end){
        $this->start = new \DateTime($start);
        $this->end = new \DateTime($end);

        return $this;
    }

    function getDatesReccuring($recurence='daily',$limit=1000){
        $date = clone $this->start;
        $in = $recurence == 'daily' ? 'D' : 'D';
        $num = $recurence == 'daily' ? 1: 7;
        $interval = new \DateInterval('P'.$num.$in);
        $results= [] ;
        $results[]= clone $date;
        while ( $date->add($interval) <= $this->end){
            
            $results[]=clone $date;
        }
        return $results;
    }

    function randomDateBetween($min,$max){
        $min_date = strtotime($min);
        $max_date = strtotime($max);
        $ts_start = mt_rand($min_date,$max_date);
        return Date::parse($ts_start)->format($this->stringFormat);
    }
    
    function fromRequest(Request $request){
        $range_start = $request->query->get('range_start');
        $range_end = $request->query->get('range_end');
        $range= $request->query->get('range');
        return $this->getDatesForRange($range_start,$range_end,$range);
    }
    function toShortArray(){
        return [$this->start,$this->end];
    }

    function previousMonth(){
        $this->start = new \DateTime($this->year."-".$this->month."-01");
        $this->start->modify('-1 month');
        $this->end= clone $this->start;
        $this->end->modify("+".($this->start->format('t')-1)." day");
        return $this;
    }

    function currentMonth(){
        $this->start  = new \DateTime($this->year."-".$this->month."-01");
        $this->end = new \DateTime($this->year."-".$this->month."-".$this->num_days);
        return $this;
    }
    
    function currentMonthToDate(){
        $this->start = new \DateTime($this->year."-".$this->month."-01");
        $this->end = new \DateTime($this->year."-".$this->month."-".$this->day);
        return $this;
    }

    function currentYearToDate(){
        $this->start = new \DateTime($this->year."-01-01");
        $this->end = new \DateTime();
        return $this;
    }

    function currentYear(){
        $this->start = new \DateTime($this->year."-01-01");
        $this->end = new \DateTime(($this->year)."-12-31");
        return $this;
    }

    function previousYear(){
        $this->start = new \DateTime(($this->year-1)."-01-01");
        $this->end = new \DateTime(($this->year-1)."-12-31");
        return  $this;
    }

    function start(){
        return $this->start;
    }
    function matchQuarter($results){
        $quarter = $results['quarter'];
        $year = $results['year'];
        $month_start = ($quarter * 3 )-2;
        $this->start = new \DateTime($year."-".$month_start."-01");
        $date = new \DateTime($year."-".($month_start+2)."-01");
        $this->end = new \DateTime($year."-".($month_start+2)."-".$date->format('t'));
        return $this;
    }
    function matchYear($results){
       
        $year = $results['year'];

        $this->start  = new \DateTime($year."-01-01");
        $this->end= new \DateTime($year."-12-31");
        return $this;
    }
    function matchMonth($results){
        $year = $results['year'];
        $month = $results['month'];
        $this->start = new \DateTime($year."-".$month."-01");

        $this->end = new \DateTime($year."-".$month."-".$date_start->format('t'));


        return $this;
    }
    function fromPreset($preset){
        $method = $preset;
        $args = [];
       



        if(!method_exists($this,$method)){

            $found = false;
            foreach($this->matches as $re => $m){
                if((preg_match($re,$preset,$results)) ){
                    $method= $m;
                    $args[]=$results;
                    
                    break;
                }
            }

            if(method_exists($this,Str::camel($method))){
                $method = Str::camel($method);
                $found = true;
            }

            if(!$found)
                throw new \Exception('preset not found');
        }

        
        return $this->$method(...$args);
    }

    function getDatesForRange($range_start=null,$range_end=null,$range=null){
        

        if($range_start && $range_end){
            $this->start = new \DateTime($range_start);
            $this->end = new \DateTime($range_end);
        }else if ($range_end){
            $this->end = new \DateTime($range_end);
        }else if ($range_start){
            $this->start = new \DateTime($range_start);
        }else if (empty($range)){
            $this->start= new \DateTime($this->year."-".$this->month."-".$this->day);
           
        }else {
            return $this->fromPreset($range);
        }



        
    
      return $this;
    
    }

}
