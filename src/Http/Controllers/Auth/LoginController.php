<?php  


namespace GKA\Backpack\Http\Controllers\Auth;


use Backpack\CRUD\app\Http\Controllers\Auth\LoginController as BackpackLoginController;
use Illuminate\Http\Request;

class LoginController extends BackpackLoginController{
    use \Backpack\CRUD\app\Library\Auth\AuthenticatesUsers{
        logout as defaultLogout;
        attemptLogin as defaultAttemptLogin;
    }

    public function __construct(){
        parent::__construct();
    }
    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
    //    dd(\Auth::guard(backpack_guard_name()),backpack_guard_name(),$this->guard());
        $auth =  $this->defaultAttemptLogin($request);
        if($auth){
           // dd(backpack_user());
           $user = backpack_user();
           if($user->force_password_change && empty($user->password_changed_on)){
                session(['weak_password' => true]);
           }else{
            session(['weak_password' => false]);
           }
        }
        return $auth;
    }

     /**
     * Get the guard to be used during logout.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return backpack_auth();
    }

}