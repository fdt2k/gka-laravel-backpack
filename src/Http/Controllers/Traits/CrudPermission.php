<?php

namespace GKA\Backpack\Http\Controllers\Traits;

/**
 * Bind permission to operation in a Backpack Crud Controller
 *  */
trait CrudPermission
{
    protected $defaultPermissionsBinding =  [
        'read' => [
            'list',
            'show',
            'preview'
        ],
        'write'=> [
            'create',
            'delete',
            'update',
            'edit'
        ]
    ];

    protected $defaultOperations = ['list','preview','show','edit','update'];



    protected function loadPermissions($prefix, $_operationsToBind = false, $_binding = false)
    {
        $binding  = $_binding  ?: $this->defaultPermissionsBinding;
        $operationsToBind = $_operationsToBind ?: $this->defaultOperations;

        collect($operationsToBind)->map(function ($bind) use ($prefix, $binding) {
            $this->crud->operation($bind, function () use ($prefix, $binding) {
                collect($binding)->map(function ($operations, $permission) use ($prefix) {
                    collect($operations)-> map(function ($operation) use ($prefix, $permission) {
                        if (backpack_user()->can("{$prefix}.{$permission}")) {
                            $this->crud->allowAccess($operation);
                        } else {
                            $this->crud->denyAccess($operation);
                        }
                    });
                });
            });
        });
    }
}
