<?php

namespace GKA\Backpack\Http\Controllers\Operations;

use Illuminate\Support\Facades\Route;

trait PreviewOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupPreviewRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/preview', [
            'as'        => $routeName.'.preview',
            'uses'      => $controller.'@preview',
            'operation' => 'preview',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupPreviewDefaults()
    {
        $this->crud->allowAccess('preview');

        $this->crud->operation('preview', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('line', 'preview', 'view', 'gka/crud::buttons.preview');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function preview($id)
    {
        $this->crud->hasAccessOrFail('preview');

        return redirect(url('pages', $this->crud->getEntry($id)->slug));
    }
}
