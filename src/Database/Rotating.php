<?php 
namespace GKA\Backpack\Database;
use DB;
use Illuminate\Support\Facades\Schema;

trait Rotating
{


    trait Rotating{
    public static function current(){
        return Setting::where('status','active')
                ->where(function ($query) {
                    $query->where('start_on', '<=', Date::now()->format('Y-m-d'))
                        ->orWhereNull('start_on');
                })
                ->where(function ($query) {
                    $query->where('end_on', '>=', Date::now()->format('Y-m-d'))
                        ->orWhereNull('end_on');
                })
                ->orderBy('start_on','desc')
                ->orderBy('end_on','desc')
                ->with('frontEvent')->with('frontEvent.contentCategory')->get()->first();
    }
}