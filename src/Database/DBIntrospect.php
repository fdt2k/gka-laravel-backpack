<?php namespace GKA\Backpack\Database;
use DB;
use Illuminate\Support\Facades\Schema;


trait DBIntrospect
{
    public static function tableName()
    {
        return with(new static)->getTable();
    }

    public static function columns($withPrefix=false,$prefix=null){

        if(empty($prefix)){
            $prefix = self::tableName();
        }

        return collect(Schema::getColumnListing(self::tableName()))->mapWithKeys(function($item) use($withPrefix,$prefix){
            $value = $item;
            if($withPrefix){
                $value = $prefix.'.'.$item;                
            }
            return [$item=>$value];
        });
    }
}
