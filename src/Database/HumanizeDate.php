<?php namespace GKA\Backpack\Database;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait HumanizeDate
{
    public static function bootHumanizeDate(): void
    {
       
    }

    protected function initializeHumanizeDate()
    {

        if (property_exists($this, 'croppable')) {
            if (in_array($key, array_keys($this->croppable))) {
          
                $this->crop($key, $value);
            }
        }
    }

   

}
