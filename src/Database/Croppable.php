<?php namespace GKA\Backpack\Database;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

trait Croppable
{
    public static function bootCroppable(): void
    {
        static::updating(function ($croppable) {

       //     dd('hello',$croppable,$croppable->croppable);
        });

        static::deleting(function ($obj) {
            if (!method_exists($obj, 'trashed')) {
                $obj->deleteExisting($this->getOriginal($attribute_name));
            }
        });
    }


    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);

        if (property_exists($this, 'croppable')) {
            if (in_array($key, array_keys($this->croppable))) {
          
                $this->crop($key, $value);
            }
        }
    }

    public function crop($attribute_name, $value)
    {
        // or use your own disk, defined in config/filesystems.php
        $disk = config('backpack.base.root_disk_name');
        // destination path relative to the disk above

        

        $destination_path = "public/uploads";
        $path_original = 'original';
        if(isset($this->croppable[$attribute_name]['path'])){
            $destination_path = $destination_path."/".trim($this->croppable[$attribute_name]['path'],'/');

        }

        $destination_path_original = $destination_path."/original";

 
        // if the image was erased
        if ($value==null) {
            // delete the image from disk
            $this->deleteExisting($this->getOriginal($attribute_name));

            // set null in the database column
            $this->attributes[$attribute_name] = null;
        } else {

            if (is_object($value)) {
                $existing = $this->getOriginal($attribute_name) ;
                $cropped_image = $value->image;
            
                $result['image']= $this->saveBase64Image($cropped_image, $destination_path, $existing['image']??null, $disk);

                $original_image = $value->original;
                $result['original']= $this->saveBase64Image($original_image, $destination_path_original, $existing['original']??null, $disk);



                $result['crop']=$value->crop;


                $this->attributes[$attribute_name]= json_encode($result);
            }
        }
    }

    public function deleteExisting($croppable)
    {
        $disk = config('backpack.base.root_disk_name');

       
        if ($croppable !== null && !empty($croppable["image"])) {
            \Storage::disk($disk)->delete('public/'.$croppable["image"]);
        }
        if ($croppable !== null && !empty($croppable["original"])) {
            \Storage::disk($disk)->delete('public/'.$croppable["original"]);
        }
    }

    public function saveBase64Image($value, $dest, $existing, $disk)
    {
        if (Str::startsWith($value, 'data:image')) {
            // 0. Make the image
            $image = \Image::make($value)->encode('jpg', 90);

            // 1. Generate a filename.
            $filename = md5($value.time()).'.jpg';

            // 2. Store the image on disk.
            \Storage::disk($disk)->put($dest.'/'.$filename, $image->stream());

            // 3. Delete the previous image, if there was one.
            //  dd($existing);
            if ($existing !==null) {
                \Storage::disk($disk)->delete('public/'.$existing);
            }
            // 4. Save the public path to the database
            // but first, remove "public/" from the path, since we're pointing to it
            // from the root folder; that way, what gets saved in the db
            // is the public URL (everything that comes after the domain name)
            $public_dest = Str::replaceFirst('public/', '', $dest);
            return $public_dest.'/'.$filename;
        }
        return $existing;
    }
}
