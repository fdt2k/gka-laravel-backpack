<?php namespace GKA\Backpack\Database;
use DB;
use Illuminate\Support\Facades\Schema;

/*
implementation of 
http://mikehillyer.com/articles/managing-hierarchical-data-in-mysql/
*/
trait Nested
{

    use DBIntrospect;


    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
 /**
     * Get all menu items, in a hierarchical collection.
     * Only supports 2 levels of indentation.
     */
    public static function getTree()
    {
        $menu = self::orderBy('lft')->get();

        if ($menu->count()) {
            foreach ($menu as $k => $menu_item) {
                $menu_item->children = collect([]);

                foreach ($menu as $i => $menu_subitem) {
                    if ($menu_subitem->parent_id == $menu_item->id) {
                        $menu_item->children->push($menu_subitem);

                        // remove the subitem for the first level
                        $menu = $menu->reject(function ($item) use ($menu_subitem) {
                            return $item->id == $menu_subitem->id;
                        });
                    }
                }
            }
        }

        return $menu;
    }

   
    public function real_depth(){
            
         $r = 
         self::select($this->table.'.id',DB::raw('COUNT(parent.name) - 1 as depth'))
            ->from(DB::raw($this->table.','.$this->table.' as parent'))
            ->whereBetween($this->table.'.lft',[DB::raw('`parent`.lft'),DB::raw('`parent`.rgt')])
            ->where($this->table.'.id',$this->id)
            ->groupBy($this->table.'.id',$this->table.'.lft')
            ->orderBy ($this->table.'.lft')
            ->first()    ;

        return $r->depth;
    }



    static public function flatNestedList($indentChar=''){
        $table = self::tableName();
        $columns = self::columns(true)->except('name')->join(',');

        $r=self::select(
                DB::raw($columns),
                DB::raw("CONCAT( REPEAT('".$indentChar."', COUNT(parent.name) - 1), ".$table.".name) as name")
            )
        ->from(DB::raw($table.','.$table.' as parent'))
        ->whereBetween($table.'.lft',[DB::raw('`parent`.lft'),DB::raw('`parent`.rgt')])
        ->groupBy($table.'.id',$table.'.lft')
        ->orderBy ($table.'.lft')
        ->get();
        //throw new \Exception('test');
        return $r;
    }

    public function allChildren($indentChar='-'){
        $table = self::tableName();
        $columns = self::columns(true)->except('name')->join(',');

        $r=self::select(
                DB::raw($columns),
                DB::raw("CONCAT( REPEAT('".$indentChar."', COUNT(parent.name) - 1), ".$table.".name) as name")
            )
        ->from(DB::raw($table.','.$table.' as parent'))
        ->where('parent.id',$this->id)
        ->where($table.'.id','!=',$this->id)
        ->whereBetween($table.'.lft',[DB::raw('`parent`.lft'),DB::raw('`parent`.rgt')])
        ->groupBy($table.'.id',$table.'.lft')
        ->orderBy ($table.'.lft')
        ->get();
        //throw new \Exception('test');
        return $r;
    }
}
