<?php 


namespace GKA\Backpack\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class JsonProperty implements CastsAttributes
{

    public function __construct(...$properties){
        $this->properties = [...$properties];
    }
    /**
     * Cast the given value.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return array
     */
    public function get($model, $key, $value, $attributes)
    {

        return $value;
        dump($this->properties,$key,$value,$attributes);

        $result = json_decode($value, true);
        
        $result = collect($result)->map(function($item,$key){
            if( is_string($item['content']) && ! empty($item['content'])){
                $item['content']=json_decode($item['content'],true);
            }
            return $item;
        })->toArray();

        return $result;
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  array  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, $key, $value, $attributes)
    {
        
      /*  dd($value);
        if(!is_string($value)){
            return  json_encode($value);
        }
*/
        return $value;
        //return json_encode($value);
    }
}