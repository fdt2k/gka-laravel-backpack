<?php

namespace GKA\Backpack\Commands;

use GKA\Noctis\Providers\AuthProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;

class FakeModel
{
}
class MigrateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gka:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fresh migration';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }


    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Publishing the Noctis assets, database, and config files');

        /*   if ($this->files->exists('./app/Models')) {
               $this->warn('Temporary renaming Models folder to keep laravel-shift/blueprint getting crazy');
               $this->files->move('./app/Models', './app/_Models');
           }*/
        if ($this->files->exists('./.blueprint')) {
            //    $this->call('blueprint:erase');
            //   $this->call('blueprint:build');
            $this->warn('blueprint already generated - erasing files');
            $this->call('blueprint:erase');
        }

        $this->call('blueprint:build');
        /*if ($this->files->exists('./app/_Models')) {
            $this->warn('Moved Models back to original folder');
            $this->files->move('./app/_Models', './app/Models');
        }*/

        foreach (glob('./app/'.config('gka.backpack.blueprint.source').'/*.php') as $filename) {
            dump($filename);
            preg_match('/([^\/]*)\.php/', $filename, $match);
            $class = $match[1];

            $this->call('gka:scaffold:blueprintmodel', ['table_name'=>$class]);
        }

        //   $this->call('backpack:build');

        $this->call('migrate:fresh', ['--seed'=>true]);
    }
}
