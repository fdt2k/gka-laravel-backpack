<?php

namespace GKA\Backpack\Commands;

use GKA\Backpack\BackpackProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class PublishConfig extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gka:publish:config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'publish config';

    /**
     * Get user options.
     */
    protected function getOptions()
    {
        return [
          //  ['create', null, InputOption::VALUE_NONE, 'Create an admin user', null],
        ];
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Publishing the available config');
        $this->call('vendor:publish', ['--provider' => BackpackProvider::class, '--tag' => ['config']]);
    }

    

    /**
     * Get command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
   //         ['email', InputOption::VALUE_REQUIRED, 'The email of the user.', null],
        ];
    }

    
}
