<?php

namespace GKA\Backpack\Commands;

use GKA\Backpack\BackpackProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class PublishFieldsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gka:publish:fields';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'publish fields templates';

    /**
     * Get user options.
     */
    protected function getOptions()
    {
        return [
          //  ['create', null, InputOption::VALUE_NONE, 'Create an admin user', null],
        ];
    }

    public function fire()
    {
        return $this->handle();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Publishing the available fields');
        $this->call('vendor:publish', ['--provider' => BackpackProvider::class, '--tag' => ['fields']]);
    }

    

    /**
     * Get command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
   //         ['email', InputOption::VALUE_REQUIRED, 'The email of the user.', null],
        ];
    }

    
}
