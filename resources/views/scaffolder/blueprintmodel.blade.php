@php echo "<?php"
@endphp


namespace {{ $namespace }};

use Cviebrock\EloquentSluggable\Sluggable;
use {{$parentClass}} as GeneratedModels;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class {{ $classname }} extends GeneratedModels\{{$classname}}
{
    use CrudTrait;
}

