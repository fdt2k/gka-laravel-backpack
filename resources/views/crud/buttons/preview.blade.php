@if ($crud->hasAccess('preview'))
<a href="{{ url($crud->route.'/'.$entry->getKey().'/preview') }}" target="_blank" class="btn btn-sm btn-link pr-0"><i class="la la-eye"></i> {{ trans('backpack::crud.preview') }}</a>
@endif