const mix = require('laravel-mix');

// copy CRUD filters JS into packages
mix
	.copy('node_modules/@editorjs/editorjs/dist', 'publishable/packages/editorjs')
	.copy('node_modules/@editorjs/header/dist/bundle.js', 'publishable/packages/editorjs/tools/header.js')
	.copy('node_modules/@editorjs/list/dist/bundle.js', 'publishable/packages/editorjs/tools/list.js')
	.copy('node_modules/@editorjs/table/dist/bundle.js', 'publishable/packages/editorjs/tools/table.js')
	.copy('node_modules/editorjs-table/dist/bundle.js', 'publishable/packages/editorjs/tools/table2.js')
	.copy('node_modules/editorjs-hyperlink-browse/dist/bundle.js', 'publishable/packages/editorjs/tools/hyperlink.js')
    .copy('node_modules/@editorjs/embed/dist/bundle.js', 'publishable/packages/editorjs/tools/embed.js')
	.copy('node_modules/editorjs-image-browse/dist/bundle.js', 'publishable/packages/editorjs/tools/simple-image.js')
